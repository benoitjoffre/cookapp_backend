let mongoose = require('mongoose');

let Schema = mongoose.Schema;

const Dislike = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    dishId: {
        type: Schema.Types.ObjectId,
        ref: 'Dish'
    },
    commentId: {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('Dislike', Dislike);