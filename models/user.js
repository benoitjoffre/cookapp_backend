const mongoose = require('mongoose')

const Schema = mongoose.Schema

const User = new Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        lowercase: true,
        index: true,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    }
})
module.exports = UserModel = mongoose.model('User', User)
