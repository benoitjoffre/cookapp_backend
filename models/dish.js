
let mongoose = require('mongoose');

let Schema = mongoose.Schema;

const Dish = new Schema({
    name: {
        type: String,
        required : [ false, 'name is required']
    },
    ingredients:{
        type:Array,
        default:[
            {
            ingredientName: String,
            quantity: String,
            imgUrl: String
            }
        ]
    },

    preparation: {
        type: Array,
        default: [
            {
                etape: Number,
                description : String
            }
        ]
    },

    preparationTime: {
        type: String
    },

    totalTime: {
        type: String
    },

    bakingTime : {
        type: String
    },

    personsNumber : {
        type: String
    },

    dishImgUrl: {
        type: String,
        required: [ false, 'url img is required']
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Dish', Dish);