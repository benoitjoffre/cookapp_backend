const puppeteer = require('puppeteer');
const fetch = require('node-fetch');

const url = 'https://www.marmiton.org/recettes/recherche.aspx?aqt=';

const getAllUrl = async (browser) => {
    const param = ['pizza']
    console.log('PRAMETERS', param)

   
    const page = await browser.newPage();
    const res = param.map(async plat => {
        await page.goto(url + plat, {waitUntil: 'load', timeout: 0})
        await page.waitFor('body')
        
        const result = await page.evaluate(() =>
        [...document.querySelectorAll('.recipe-card-link')].map(link => link.href),
        )
        
        return result
      })
     return await Promise.all(res)
}

const getDataFromUrl = async (browser, url) => {
  console.log(url)
  const result = url.map(async u => {
    try {

      const page = await browser.newPage()
      await page.goto(u,  {waitUntil: 'load', timeout: 0})
      await page.waitFor('body')
      return page.evaluate( () => {
        let name = document.querySelector('.main-title') && document.querySelector('.main-title').innerText;
        let ingredientsList = [...document.querySelectorAll('.recipe-ingredients__list__item')]
        const ingredients = ingredientsList.map(ingredient => {
          let res = {
            ingredientName: '',
            quantity: '',
            imgUrl: '',
          }
          res.imgUrl = ingredient.querySelector('.ingredients-list__item__icon ') && ingredient.querySelector('.ingredients-list__item__icon ').getAttribute('src')
          res.ingredientName = ingredient.querySelector('.ingredient') && ingredient.querySelector('.ingredient').innerText
          res.quantity =  ingredient.querySelector('.recipe-ingredient-qt') && ingredient.querySelector('.recipe-ingredient-qt').innerText
          return res 
        })
        let dishImgUrl = document.querySelector('#af-diapo-desktop-0_img') && document.querySelector('#af-diapo-desktop-0_img').getAttribute('src');
        let etapesList = [...document.querySelectorAll('.recipe-preparation__list__item')]
        const preparation = etapesList.map((etape, i) => {
          let res = {
            etape: '',
            description: '',
          }
          res.etape = i 
          res.description = etape && etape.innerText
          return res
        })
        const preparationTime = document.querySelector('.recipe-infos__timmings__preparation') && document.querySelector('.recipe-infos__timmings__preparation').innerText 
        const totalTime = document.querySelector('.recipe-infos__timmings__total-time') && document.querySelector('.recipe-infos__timmings__total-time').innerText 
        const bakingTime = document.querySelector('.recipe-infos__timmings__cooking') && document.querySelector('.recipe-infos__timmings__cooking').innerText 
        const personsNumber = document.querySelector('.recipe-infos__quantity__value') && document.querySelector('.recipe-infos__quantity__value').innerText 
        
        return { name, ingredients, dishImgUrl, preparation, preparationTime, totalTime, bakingTime , personsNumber }
      })
      } catch (error) {
        browser.close()
        console.error('erreur map result', error)
      }
  })
  return await Promise.all(result)
}


const scrap = async () => {
  const browser = await puppeteer.launch({ headless: true})
  const urlList = await getAllUrl(browser)

  const results = await Promise.all(
    urlList.map(url => getDataFromUrl(browser, url)),
  )
browser.close()
 
  return results
}


scrap()
  .then(value => {
    value[0].map(val => {
      console.log('val é riz JSON', val.length)
      try {
        fetch('https://immense-coast-52319.herokuapp.com/api/v1/dishes', {
        method: 'post',
        body:    JSON.stringify(val),
        headers: { 'Content-Type': 'application/json' },
    })
    .then(res => res.json())
    .then(json => console.log("ok", json));
      } catch(err) {
        console.log('TRYCATCHERROR', err)
      }
      
    })
    

    // console.log('value', value)
  })
  .catch(e => console.log(`error: ${e}`))
