
const express = require('express');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require('../../models/user');

const authConfig = require("../../configs/config/authConfig")
const validateRegisterInput = require("../../validations/authentication/register");
const validateLoginInput = require("../../validations/authentication/login");

const register = async (req, res) => {
     // Validation du formulaire
  const { errors, isValid } = validateRegisterInput(req.body);
  // Vérifier que le formulaire soit valide
    if (!isValid) {
      return res.status(400).json(errors);
    }
  await User.findOne({ email: req.body.email }).then(user => {
      if (user) {
        return res.status(400).json({ email: "Email already exists" });
      } else {
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password
        });
  // Crypter le mot de passe avant de l'envoyer en BDD
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => res.json(user))
              .catch(err => console.log(err));
          });
        });
      }
    });
}

const login = async (req, res) => {
     try {
         // Form validation
         const { errors, isValid } = validateLoginInput(req.body);
         // Check validation
         if (!isValid) {
             return res.status(400).json(errors);
         }
         const email = req.body.email;
         const password = req.body.password;
         // Chercher l'utilisateur par email
         await User.findOne({ email }).then(user => {
             // Vérifier si l'utilisateur existe
             if (!user) {
             return res.status(404).json({ emailnotfound: "Email not found" });
             }
         // vérifier le mot de passe
             bcrypt.compare(password, user.password).then(isMatch => {
             if (isMatch) {
                 // Utilisateur trouvé
                 // Creation du payload JWT
                 const payload = {
                 id: user.id,
                 name: user.name
                 };
         // Sign token
                 jwt.sign(
                 payload,
                 authConfig.secretOrKey,
                 {
                     expiresIn: 3600000 // 1h
                 },
                 (err, token) => {
                     res.json({
                     success: true,
                     token: "Bearer " + token
                     });
                 }
                 );
             } else {
                 return res.status(400).json({ passwordincorrect: "Password incorrect" });
             }
             });
         });
     } catch(err) {
       console.log(err)
     }
}


module.exports = {
    register: register,
    login: login
    
}