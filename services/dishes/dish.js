
const express = require('express');
const Dish = require('../../models/dish');
const User = require('../../models/user')
const getDishes = async (req, res) => {
    try {

        let dishes = await Dish.find({});

        if (dishes.length > 0) {
            return res.status(200).json({
                'message': 'dishes fetched successfully',
                'data': dishes
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'description': 'No dishes found in the system'
        });
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'description': 'something went wrong, Please try again'
        });
    }
}

const getDishById = async (req, res) => {
    try {
        let dish = await Dish.findById(req.params.id);
        if (dish) {
            return res.status(200).json({
                'message': `dish with id ${req.params.id} fetched successfully`,
                'data': dish
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'description': 'No dishes found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'description': 'something went wrong, Please try again'
        });
    }
}

const createDish = async (req, res) => {
    try {

        const {
            name,
            ingredients,
            likes,
            dishImgUrl,
            preparation,
            preparationTime,
            totalTime,
            bakingtime,
            personsNumber
        } = req.body;

        if (name === undefined || name === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'description': 'name is required',
                'field': 'name'
            });
        }

      

        if (ingredients === undefined || ingredients === '') {
            return res.status(422).json({
                'code': 'REQUIRED_FIELD_MISSING',
                'description': 'ingredients is required',
                'field': 'ingredients'
            });
        }
        
        let isNameExists = await Dish.findOne({
            "name": name
        });

        if (isNameExists) {
            return res.status(409).json({
                'code': 'ENTITY_ALREAY_EXISTS',
                'description': 'name already exists',
                'field': 'name'
            });
        }

        const temp = {
            name: name,
            ingredients : ingredients,
            dishImgUrl : dishImgUrl,
            likes : likes,
            preparation : preparation,
            preparationTime : preparationTime,
            totalTime: totalTime,
            bakingtime : bakingtime,
            personsNumber : personsNumber
        }
        console.log(temp)
        let newDish = await Dish.create(temp);

        if (newDish) {
            return res.status(201).json({
                'message': 'dish created successfully',
                'data': newDish
            });
        } else {
            throw new Error('something went worng');
        }
    } catch (error) {
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'description': 'something went wrong, Please try again',
            'message': error
        });
    }
}

const deleteDish = async (req, res, next) => {
    try {
        let dish = await Dish.findByIdAndRemove(req.params.id);
        if (dish) {
            return res.status(204).json({
                'message': `dish with id ${req.params.id} deleted successfully`
            });
        }

        return res.status(404).json({
            'code': 'BAD_REQUEST_ERROR',
            'description': 'No dishes found in the system'
        });

    } catch (error) {

        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'description': 'something went wrong, Please try again'
        });
    }
}

module.exports = {
    getDishes: getDishes,
    getDishById: getDishById,
    createDish: createDish,
    deleteDish: deleteDish
}