const Validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateLoginInput(data) {
  let errors = {};


  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";

// Regarde si l'email n'est pas vide et si il est correct
  if (Validator.isEmpty(data.email)) {
    errors.email = "L'email est obligatoire";
  } else if (!Validator.isEmail(data.email)) {
    errors.email = "Votre email est invalide";
  }
  
// Regarde si le mot de passe n'est pas vide et si il est correct
  if (Validator.isEmpty(data.password)) {
    errors.password = "Le mot de passe est obligatoire";
  }
return {
    errors,
    isValid: isEmpty(errors)
  };
};
