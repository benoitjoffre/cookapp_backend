const dishController = require('../../controllers/apis/dish');
const userController = require('../../controllers/apis/user');
const likeController = require('../../controllers/apis/like');

const express = require('express');
let router = express.Router();
router.use('/dishes', dishController);
router.use('/users', userController);
router.use('/likes', likeController);

module.exports = router;