const express = require('express');
const userService = require('../../services/users/user');
let router = express.Router();

router.post('/register', userService.register);
router.post('/login', userService.login);

module.exports = router;