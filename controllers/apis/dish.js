
const express = require('express');
const dishService = require('../../services/dishes/dish');
let router = express.Router();

router.get('/', dishService.getDishes);

router.get('/:id', dishService.getDishById);

router.post('/', dishService.createDish);

router.delete('/:id', dishService.deleteDish);

module.exports = router;