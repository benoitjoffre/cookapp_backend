const express = require('express');
const likeService = require('../../services/likes/like');
let router = express.Router();

router.post('/getLikes', likeService.getLikes);

router.post('/getDislikes', likeService.getDislikes);

router.post('/like', likeService.like);

router.post('/unLike', likeService.unLike);

router.post('/unDislike', likeService.unDislike)

router.post('/dislike', likeService.dislike)

module.exports = router;